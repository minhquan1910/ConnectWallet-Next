import { useEffect, useState } from 'react'
import { hooks, metaMask } from '../../connectors/metaMask'
import { Card } from '../Card'
import { getAddChainParamsHex } from '../../chains'
import { createLinkWalletSignature } from './createSig'

const { useChainId, useAccounts, useIsActivating, useIsActive, useProvider, useENSNames } = hooks

export default function MetaMaskCard() {
  const chainId = useChainId()
  const accounts = useAccounts()
  const isActivating = useIsActivating()

  const isActive = useIsActive()

  const provider = useProvider()
  const ENSNames = useENSNames(provider)

  const [error, setError] = useState(undefined)
  const [spawnBug, setSpawnBug] = useState(false)

  // attempt to connect eagerly on mount
  useEffect(() => {
    void metaMask.connectEagerly().catch(() => {
      console.debug('Failed to connect eagerly to metamask')
    })
  }, [])

  const switchNetwork = () => {
    const params = getAddChainParamsHex(97)
    provider.send('wallet_addEthereumChain', [params]).then(console.log).catch(console.error)
  }

  const signData = async () => {
    if (spawnBug) {
      const response = await fetch('https://catfact.ninja/fact')
      const data = await response.json()
      console.log('cats response', data)
    }
    const msgParams = createLinkWalletSignature(97, accounts[0], "1000000000000000000")
    console.log('msgParams', msgParams)
    const res = await provider.send('eth_signTypedData_v4', [accounts[0], JSON.stringify(msgParams)])
    console.log('res', res)
  }

  return (
    <>
      <Card
        connector={metaMask}
        chainId={chainId}
        isActivating={isActivating}
        isActive={isActive}
        error={error}
        setError={setError}
        accounts={accounts}
        provider={provider}
        ENSNames={ENSNames}
      />
      <div style={{ display: 'flex', flexDirection: 'column' }}>
          <button onClick={switchNetwork}>switch network</button>
          <div style={{ border: '1px solid black', margin: '12px 0', padding: '12px 6px' }}>
            <input type="checkbox" onChange={() => setSpawnBug(!spawnBug)} style={{ display: 'inline' }} />
            <p style={{ display: 'inline' }}>
              Cause iOS app store redirection issue for "sign data" request below (RPC API "eth_signTypedData_v4")
            </p>
          </div>
          <button onClick={signData}>sign data</button>
      </div>
    </>
  )
}
